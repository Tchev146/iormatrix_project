package com.kevinkerboit.iormatrix_android;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.util.Log;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;

import java.net.URISyntaxException;

public class MainActivity extends AppCompatActivity {

    Socket socket;
    PrintStream ps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onConnClicked(View v){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    socket = new Socket("10.0.2.2", 3000);
                    ps = new PrintStream(socket.getOutputStream());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void onExitClicked(View v){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    ps.println("exit");
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void onUpClicked(View v){
        new Thread(new Runnable() {
            @Override
            public void run() {
                ps.println("h");
            }
        }).start();
    }

    public void onDownClicked(View v){
        new Thread(new Runnable() {
            @Override
            public void run() {
                ps.println("b");
            }
        }).start();
    }

    public void onLeftClicked(View v){
        new Thread(new Runnable() {
            @Override
            public void run() {
                ps.println("g");
            }
        }).start();
    }

    public void onRigthClicked(View v){
        new Thread(new Runnable() {
            @Override
            public void run() {
                ps.println("d");
            }
        }).start();
    }

    public void onCenterClicked(View v){
        Log.i("info", "testCenterClicked");
        new Thread(new Runnable() {
            @Override
            public void run() {
                ps.println("init");
            }
        }).start();

    }
}
