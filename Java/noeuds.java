import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.io.PrintStream;

public class noeuds {

	public static void main(String[] args) {

		new Thread() {
			@Override
			public void run() {
				System.out.println("IoRMatrix SERVEUR Start ...");				
				try {
					boolean ready = true;
					ServerSocket serveur = new ServerSocket(3000);
					Socket Commande = null;
					Socket Matrice = null;
					while(ready) {
						Commande = serveur.accept();
						System.out.println("Commande connecte ...");
						Matrice = serveur.accept();
						System.out.println("Matrice connecte ...");
						BufferedReader br = new BufferedReader(new InputStreamReader(Commande.getInputStream()));
						PrintStream ps = new PrintStream(Matrice.getOutputStream());
						String s ;
						while(!(s = br.readLine()).equals("exit")) {
							ps.println(s);
						}
						ps.println("exit");
						ps.close();
						
					}
					Commande.close();
					Matrice.close();
					serveur.close();
				}
				catch(Exception e) {}
				
			}
		}.start();

	}

}
