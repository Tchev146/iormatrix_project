#include <MaxMatrix.h>
int DIN = 7;
int CS = 6;
int CLK= 5;

MaxMatrix matrice(DIN, CS, CLK, 1);

int x = 3;
int y = 3;

void setup() {
  matrice.init();
  matrice.setIntensity(5);
  Serial.begin(9600);
  matrice.setDot(x,y,true);
}

//><
void loop() {
  if(Serial.available() > 0){
    char c = Serial.read();
    if(c=='d') x++;
    if(c=='g') x--;
    if(c=='b') y++;
    if(c=='h') y--;
    if(x>7) x=0;
    if(x<0) x=7;
    if(y>7) y=0;
    if(x<0) y=7;
    if(c=='i') matrice.init();
    matrice.clear();
    matrice.setDot(x,y,true);
  }
}
