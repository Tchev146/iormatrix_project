#include <MaxMatrix.h>
int DIN = 7;
int CS = 6;
int CLK= 5;

MaxMatrix matrice(DIN, CS, CLK, 1);

int x = 3, x1=5;
int y = 3, y1=5;
int x0= 4, y0=4;

void setup() {
  matrice.init();
  matrice.setIntensity(5);
  Serial1.begin(9600);
  matrice.setDot(x0,y0,true);
  matrice.setDot(x,y,true);
  matrice.setDot(x1,y1,true);
}

//><
void loop() {
  if(Serial1.available() > 0){
    char c = Serial1.read();
    if(c=='d') x++;
    if(c=='g') x--;
    if(c=='b') y++;
    if(c=='h') y--;
    if(x>7) x=0;
    if(x<0) x=7;
    if(y>7) y=0;
    if(x<0) y=7;
    
    /*--- second point ---*/
    if(c=='6') x1++;
    if(c=='4') x1--;
    if(c=='5') y1++;
    if(c=='8') y1--;
    if(x1>7) x1=0;
    if(x1<0) x1=7;
    if(y1>7) y1=0;
    if(x1<0) y1=7;
    if(c=='i') matrice.init();
    setDot();
    check();
  }
}

void setDot(){
  matrice.clear();
  matrice.setDot(x0,y0,true);
  matrice.setDot(x,y,true);
  matrice.setDot(x1,y1,true);
}

void goal(){
  int valX=0, valY =0;
  valX = random(0,7);
  valY = random(0,7);
  x0 = valX;
  y0 = valY;
  setDot();
}

void check(){
  if ((x==x0) && (y==y0)) goal();
  if ((x1==x0) && (y1==y0)) goal(); 
}
