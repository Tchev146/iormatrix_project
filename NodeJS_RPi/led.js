var gpio = require('onoff').Gpio;
var pin = new gpio(2, 'out');
var blink = setInterval(unzero, 500);
setTimeout(fin, 5000);

function unzero() {
	if (pin.readSync() == 0)
		pin.writeSync(1);
	else
		pin.writeSync(0);
}

function fin() {
	clearInterval(blink);
	pin.writeSync(0);
	pin.unexport();
}
