package com.kerboitkevin;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class MainActivity extends AppCompatActivity {

    MyMatrix matrice;
    Socket socket;
    BufferedReader br;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        matrice = findViewById(R.id.matrice);
        matrice.init();
        matrice.cacher();
    }

    public void onConnClicked(View v){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    socket = new Socket("10.0.2.2", 3000);
                    br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    String s ;
                    matrice.afficher();
                    while(!(s = br.readLine()).equals("exit")) {
                        if(s.equals("d")) { matrice.incX(); }
                        if(s.equals("g")) { matrice.decX(); }
                        if(s.equals("b")) { matrice.incY(); }
                        if(s.equals("h")) { matrice.decY(); }
                        if(s.equals("init")) { matrice.init(); }
                        if(s.equals("afficher")) { matrice.afficher(); }
                        if(s.equals("cacher")) { matrice.cacher(); }
                    }
                    socket.close();
                    socket = null;
                    matrice.cacher();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
