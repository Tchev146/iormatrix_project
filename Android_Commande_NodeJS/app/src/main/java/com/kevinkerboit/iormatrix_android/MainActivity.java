package com.kevinkerboit.iormatrix_android;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import io.socket.client.IO;
import io.socket.client.Socket;


import java.net.URISyntaxException;

public class MainActivity extends AppCompatActivity {

    Socket socket;
    String player = "2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onConnClicked(View v){
        try {
            socket = IO.socket("http://kevinkerboit.com:8000");
            socket.connect();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void onExitClicked(View v){
        socket.emit("a", "01", player, "exit");
        socket.close();
    }

    public void onUpClicked(View v){
        socket.emit("a", "01", player, "h");
    }

    public void onDownClicked(View v){
        socket.emit("a", "01", player, "b");
    }

    public void onLeftClicked(View v){
        socket.emit("a", "01", player, "g");
    }

    public void onRigthClicked(View v){
        socket.emit("a", "01", player, "d");
    }

    public void onCenterClicked(View v){
        socket.emit("a", "01", player, "i");
    }
}
