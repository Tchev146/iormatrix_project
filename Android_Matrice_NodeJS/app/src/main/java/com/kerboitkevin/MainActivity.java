package com.kerboitkevin;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import java.io.IOException;
import java.net.URISyntaxException;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;


public class MainActivity extends AppCompatActivity {

    MyMatrix matrice;
    Socket socket;
    String text = "";
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        matrice = findViewById(R.id.matrice);
        textView = findViewById(R.id.textView);
        matrice.init();
        matrice.posPointFIxe();
        matrice.cacher();
    }

    public void onConnClicked(View v){
        try {
            socket = IO.socket("http://kevinkerboit.com:8000");
            socket.connect();
            matrice.afficher();
            socket.on("b", new Emitter.Listener() {
                @Override
                public void call(Object... args) {

                    if (args[0].equals("01"))
                    {
                        String s = args[2].toString();
                        if(s.equals("d") && text.equals("")) { matrice.incX(args[1].toString()); }
                        if(s.equals("g") && text.equals("")) { matrice.decX(args[1].toString()); }
                        if(s.equals("b") && text.equals("")) { matrice.incY(args[1].toString()); }
                        if(s.equals("h") && text.equals("")) { matrice.decY(args[1].toString()); }
                        if(s.equals("i")) { matrice.init(); matrice.posPointFIxe(); resetTextView(); matrice.resetPoint();}
                        if(s.equals("afficher")) { matrice.afficher(); }
                        if(s.equals("cacher")) { matrice.cacher(); }
                        if(s.equals("exit")) {
                            socket.close();
                            socket = null;
                            matrice.cacher();
                            matrice.init();
                            matrice.posPointFIxe();
                            resetTextView();
                            matrice.resetPoint();
                        }
                        matrice.checkWinCoord(args[1].toString());
                        text = matrice.checkWin();

                        if (!text.equals("")){
                            afficherText();
                        }

                    }
                }
            });

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void resetTextView() {
        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                text = "";
                textView.setVisibility(TextView.INVISIBLE);
                textView.setText(text);
            }
        });
    }

    private void afficherText() {
        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textView.setVisibility(TextView.VISIBLE);
                textView.setText(text);
            }
        });
    }

}
