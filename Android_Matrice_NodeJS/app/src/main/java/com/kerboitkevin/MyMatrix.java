package com.kerboitkevin;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import java.util.Random;

public class MyMatrix extends View {

    private Paint paintMatice ;
    private Paint paintDisque1 ;
    private Paint paintDisque2 ;
    private Paint paintDisque3 ;
    private Paint paintDisque4 ;
    private Paint paintDisque5 ;
    private Paint paintDisque6 ;
    private int n = 8 ;

    /* Coord P1 */
    private int x1 = 0 ;
    private int y1 = 0 ;

    /* Coord PFixe */
    private int x2 = 0 ;
    private int y2 = 0 ;

    /* Coord P2 */
    private int x3 = 0 ;
    private int y3 = 0 ;

    /* Score P1 */
    private int scoreP1 = 0;

    /* Score P2 */
    private int scoreP2 = 0;

    private boolean afficherBalle = true ;



    public MyMatrix(Context context) {
        super(context);
        initialize();
    }

    public MyMatrix(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public MyMatrix(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    public MyMatrix(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initialize();
    }

    private void initialize(){
        paintMatice = new Paint();
        paintMatice.setColor(Color.GRAY);


        /* */
        paintDisque1 = new Paint();
        paintDisque1.setAntiAlias(true);
        paintDisque1.setStyle(Paint.Style.FILL);
        paintDisque1.setColor(Color.rgb(0,0,0));

        paintDisque2 = new Paint();
        paintDisque2.setAntiAlias(true);
        paintDisque2.setStrokeWidth(2);
        paintDisque2.setStyle(Paint.Style.STROKE);
        paintDisque2.setColor(Color.rgb(80,80,60));

        /* */
        paintDisque3 = new Paint();
        paintDisque3.setAntiAlias(true);
        paintDisque3.setStyle(Paint.Style.FILL);
        paintDisque3.setColor(Color.rgb(255,0,0));

        paintDisque4 = new Paint();
        paintDisque4.setAntiAlias(true);
        paintDisque4.setStrokeWidth(2);
        paintDisque4.setStyle(Paint.Style.STROKE);
        paintDisque4.setColor(Color.rgb(80,80,60));

        /* */
        paintDisque5 = new Paint();
        paintDisque5.setAntiAlias(true);
        paintDisque5.setStyle(Paint.Style.FILL);
        paintDisque5.setColor(Color.rgb(0,0,255));

        paintDisque6 = new Paint();
        paintDisque6.setAntiAlias(true);
        paintDisque6.setStrokeWidth(2);
        paintDisque6.setStyle(Paint.Style.STROKE);
        paintDisque6.setColor(Color.rgb(80,80,60));
    }

    @Override
    protected void onDraw(Canvas canvas) {
        int w = getWidth() - getWidth() % n;
        int h = getHeight() - getHeight() % n;

        int pasW = w / n;
        int pasH = h / n;

        for(int i = 0; i<=w ; i+=pasW){
            canvas.drawLine(i, 0, i, h, paintMatice);
        }

        for(int j = 0; j<=h ; j+=pasH){
            canvas.drawLine(0, j, w, j, paintMatice);
        }

        if(afficherBalle) {
            canvas.drawOval(pasW*x1+4, pasH*y1+4, pasW*(x1+1)-4, pasH*(y1+1)-4, paintDisque1);
            canvas.drawOval(pasW*x1+4, pasH*y1+4, pasW*(x1+1)-4, pasH*(y1+1)-4, paintDisque2);
            canvas.drawOval(pasW*x2+4, pasH*y2+4, pasW*(x2+1)-4, pasH*(y2+1)-4, paintDisque3);
            canvas.drawOval(pasW*x2+4, pasH*y2+4, pasW*(x2+1)-4, pasH*(y2+1)-4, paintDisque4);
            canvas.drawOval(pasW*x3+4, pasH*y3+4, pasW*(x3+1)-4, pasH*(y3+1)-4, paintDisque5);
            canvas.drawOval(pasW*x3+4, pasH*y3+4, pasW*(x3+1)-4, pasH*(y3+1)-4, paintDisque6);
        }

    }

    public void posPointFIxe() {
        int r = 0;
        Random rand = new Random();
        r = rand.nextInt(n);
        x2 = r;
        r = rand.nextInt(n);
        y2 = r;
        invalidate();
    }

    public void incX(String player) {
        if (player.equals("1")) {
            if (++x1 > n - 1) x1 = 0;
        }
        if (player.equals("2")) {
            if (++x3 > n - 1) x3 = 0;
        }
        invalidate();
    }

    public void decX(String player) {
        if (player.equals("1")) {
            if (--x1 < 0) x1 = n - 1;
        }
        if (player.equals("2")) {
            if (--x3 < 0) x3 = n -1;
        }
        invalidate();
    }

    public void incY(String player) {
        if (player.equals("1")) {
            if (++y1 > n - 1) y1 = 0;
        }
        if (player.equals("2")) {
            if (++y3 > n - 1) y3 = 0;
        }
        invalidate();
    }

    public void decY(String player) {
        if (player.equals("1")) {
            if (--y1 < 0) y1 = n - 1;
        }
        if (player.equals("2")) {
            if (--y3 < 0) y3 = n - 1;
        }
        invalidate();
    }

    public void init() {
        x1 = y1 = n/2+n%2-1;
        x3 = y3 = n/4+n%4-1;
        invalidate();
    }

    public void afficher() {
        afficherBalle = true ;
        invalidate();
    }

    public void cacher() {
        afficherBalle = false ;
        invalidate();
    }

    public void checkWinCoord(String player) {
        boolean changePointFixe = false;
        if(player.equals("1") && ((x1 == x2) && (y1 == y2))){
            scoreP1++;
            changePointFixe = true;
        }
        if(player.equals("2") && ((x3 == x2) && (y3 == y2))){
            scoreP2++;
            changePointFixe = true;
        }
        if(changePointFixe){
            posPointFIxe();
            invalidate();
        }
    }

    public String checkWin(){

        String retour = "";

        if (scoreP1 >= 10){
            retour = "P1 WIN ! ";
        }

        if (scoreP2 >= 10){
            retour = "P2 WIN ! ";
        }

        return retour;
    }

    public void resetPoint(){
        scoreP1 = 0;
        scoreP2 = 0;
    }

}