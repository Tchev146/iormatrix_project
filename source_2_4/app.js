var app = require('express')();
var server = require('http').createServer(app);

app.get('/', function (req, res) {
  res.sendFile(__dirname + '/index.html');
});

app.get('/c', function (req, res) {
  res.sendFile(__dirname + '/commande.html');
});

var io = require('socket.io').listen(server);

/*
io.sockets.on('connection', function (socket) {
   io.emit('c', "init");
   socket.on('xmodif', function (message) {
   	io.emit('x', message);
   });
   socket.on('ymodif', function (message) {
   	io.emit('y', message);
   });
});
*/
server.listen(8080);
